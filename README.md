# README #


### What is this repository for? ###

* Repository which contains a java project to classify or a patients is likely to be readmitted to the hospital. Makes use of a weka model.
* Version 2020

### How do I get set up? ###

* Clone repository and open it in Intellij
* The project can be used by running the WekkaRunner.


### Who do I talk to? ###

* Britt Vink
* b.vink@st.hanze.nl

see https://bitbucket.org/bvink1/project_thema_9/src/master/ for more information about the background 